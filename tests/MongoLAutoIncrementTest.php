<?php 

/**
 * Class MongoLAutoIncrementTest
 * @author hwz
 */
class MongoLAutoIncrementTest extends PHPUnit_Framework_TestCase {

    /**
     * setUp
     * @return void
     * @author hwz
     **/
    public function setUp() {
        // include('Models/Order.php');
        new MongoLConnection(array('host'=>'localhost','port'=>27017,'dbname'=>'users'));
    }
    /**
     * testSeq
     * @return void
     * @author hwz
     **/
    public function testSeq() {
        $seq = MongoLAutoIncrement::seq('hello');
        $this->assertEquals(1,$seq);

        $seq = MongoLAutoIncrement::seq('hello');
        $this->assertEquals(2,$seq);

        $seq = MongoLAutoIncrement::seq('hello');
        $this->assertEquals(3,$seq);

        $seq = MongoLAutoIncrement::seq('hello');
        $this->assertEquals(4,$seq);
        // MongoLAutoIncrement::find('hello')->delete();

        // include('Models/Increment.php');
        $in = new Increment();
        $in->name = 'hello';
        $in->save();
        $seq = MongoLAutoIncrement::seq('increment');
        $this->assertEquals(2,$seq);
    }
}


//end of file
