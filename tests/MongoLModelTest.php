<?php 

/**
 * Class MongoLModel
 * @author hwz
 */ class MongoLModelTest extends PHPUnit_Framework_TestCase
{

    protected $condition = array(
        'skip'=>'',
        'take'=>'',
        'where'=>'',
        'fields'=>'',
    );

    /**
     * tearUp
     * @return void
     * @author hwz
     **/
    public function setUp() {
        
        /* if (!class_exists('User')) {
            include('Models/User.php');
            include('Models/Profile.php');
        }

        if (!class_exists('Order')) {
            include('Models/Order.php');
        } */

        new MongoLConnection(array('host'=>'localhost','port'=>27017,'dbname'=>'users'));
    }

    /**
     * testSave
     * @return void
     * @author hwz
     **/
    public function testSave() {

        $user = new User();
        $user->phone = rand(100000000000,2000000000000);
        $r = $user->save(array('username'=>'coderhwz'));
        $this->assertTrue($r);
        $this->assertTrue($user->_id instanceof MongoId);
        $r = $user->delete();
        $this->assertTrue($r == 1); 

        $u1 = new User();
        $r = $u1->delete();
        $this->assertEquals(0,$r);

        /* $user1 = new User();

        $user1->_id = rand();
        $user1->phone = '15158128077';
        $user1->username = 'coderhwz-991';
        $status = $user1->save();

        $this->assertEquals(1,$status);
        
        $this->assertTrue(is_integer($user1->_id)); */
    }

    /**
     * testFind
     * @return void
     * @author hwz
     **/
    public function testFind() {
        $user = new User();
        $user->phone = (string)rand();
        $user->username = 'coderhwz' . rand();
        $user->save();

        $check = User::find($user->_id,array('username'));

        $this->assertInstanceOf('MongoLModel',$check);

        User::where(array('_id'=>$check->_id))->delete();


        /* $users = User::skip(2)->take(1)->get();
        var_export($users); */

        /* $user1 = new User();
        $user1->phone = 123;
        $user1->nickname = 'update-me';
        $user1->save();

        $user1->nickname = 'update-me-updated';
        $user1->save();

        $user2 = User::find($user1->_id);
        $this->assertEquals('update-me-updated',$user2->nickname);

        $user3 = User::find($user1->_id,'nickname phone');
        $this->assertEquals('update-me-updated',$user3->nickname);
        $this->assertEquals(123,$user3->phone); */
 
    }

    /**
     * testHasOne
     * @return void
     * @author hwz
     **/
    public function testHasOne() {

        $user = new User();
        $user->username="coderhwz@gmail.com";
        $user->phone = "15158128099";
        $user->save(); 


        $profile = new Profile();
        $profile->account = "china bank";
        $profile->user_id = $user->_id;
        $r = $profile->save();



        $u = User::find($user->_id,'_id');
        $this->assertInstanceOf('Profile',$u->profile);
        $this->assertEquals('china bank',$u->profile->account);
        $this->assertInstanceOf('Profile',$u->profile()->get());
        $u->profile->delete();
        $u->delete();
        
    }

    /**
     * testHasMany
     * @return void
     * @author hwz
     **/
    public function testHasMany() {

        $user = $this->newUser();

        for ($i = 0; $i < 10; $i++) {
            $order = new Order();
            $order->goodsname = '笔记本';
            $order->order_sn = $i;
            $order->user_id = $user->_id;
            $order->save();
        }

        $this->assertInstanceOf('MongoLCollection',$user->orders);
        $this->assertInstanceOf('MongoLQueryBuilder',$user->orders());
        $this->assertEquals(10,$user->orders->count());
        $user->orders()->delete();

        $user->delete();
    }

    /**
     * testBelongsTo
     * @return void
     * @author hwz
     **/
    public function testBelongsTo() {
        $user = $this->newUser();
        $order = new Order();
        $order->goodsname = '笔记本';
        $order->order_sn = '00000';
        $order->user_id = $user->_id;
        $order->save();
        // var_export($order->_id);

        $this->assertInstanceOf('User',$order->ownner);
        // var_export($order->ownner->toArray());

        $order->delete();
        $user->delete();
    }

    /**
     * newUser
     * @return void
     * @author hwz
     **/
    private function newUser() {
        $user = new User();
        $user->username =  "coderhwz@gmail.com";
        $user->phone = "15158128049";
        $user->save();
        $this->assertEquals(time(),$user->addtime);
        return $user;
    }

    /**
     * testChanged
     * @return void
     * @author hwz
     **/
    public function testChanged() {
        $user = $this->newUser();
        $user->phone = '123';
        $user->save();
        $r = $user->changed();

        $user->delete();
    }

    /**
     * testIsset
     * @return void
     * @author hwz
     **/
    public function testIsset() {
        $user = $this->newUser();
        $user->phone = '123';
        $user->save();

        $u = User::find($user->_id);
        $this->assertTrue(isset($u->_id));
        $user->delete();
    }

}


//end of file
