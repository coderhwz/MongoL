<?php 

function MongoLLoader($file){
    if (stripos($file,'MongoL') === 0) {
        require_once("src/$file.php");
    }
}

spl_autoload_register('MongoLLoader');

//end of file
