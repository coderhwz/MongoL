<?php 

/**
 * Class MongoLAutoIncrementTest
 * @author hwz
 */
class MongoLCollectionTest extends PHPUnit_Framework_TestCase {

    public $collection;
    /**
     * setUp
     * @return void
     * @author hwz
     **/
    public function setUp() {
        $data = array(
            array('id'=>1,'money'=>100),
            array('id'=>1,'money'=>100),
            array('id'=>1,'money'=>100),
            array('id'=>1,'money'=>100),
            array('id'=>1,'money'=>100),
        );
        $this->collection = new MongoLCollection($data,'stdClass');
    }

    /**
     * testColumn
     * @return void
     * @author hwz
     **/
    public function testColumn() {
        $array = $this->collection->column('money');
        $this->assertEquals(array(
            100,100,100,100,100
        ),$array);
    }

    /**
     * testSum
     * @return void
     * @author hwz
     **/
    public function testSum() {
        $sum = $this->collection->sum('money');
        $this->assertEquals($sum,500);
    }

}


//end of file
