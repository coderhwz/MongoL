<?php 

/**
 * Class MongoLConnectionTest
 * @author hwz
 */
class MongoLConnectionTest extends PHPUnit_Framework_TestCase {

    /**
     * testCreate
     * @return void
     * @author hwz
     **/
    public function testCreate() {
        $config = array(
            'host'=>'localhost',
            'port'=>27017,
            'dbname'=>'users',
        );

        $conn = new MongoLConnection($config,'users');

        $conn1 = MongoLConnection::get('users');

        $this->assertTrue($conn === $conn1);

        // var_export($conn->database);
    }
}


//end of file
