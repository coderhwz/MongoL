<?php 


/**
 * Class MongoLQueryBuilderTest
 * @author hwz
 */
class MongoLQueryBuilderTest extends PHPUnit_Framework_TestCase {
    
    private $user;
    private $query;
    /**
     * setUp
     * @return void
     * @author hwz
     **/
    public function setUp() {
        $this->user = new User();
        $this->query = $this->user->newQuery();
    }

    /**
     * testSave
     * @return void
     * @author hwz
     **/
    public function testSave() {
        $doc = array('phone'=>123,'username'=>'coderhwz');
        $result = $this->query->save($doc);

        $this->assertTrue(is_integer($doc['updatetime']));
        $this->assertTrue(is_integer($doc['addtime']));
        $this->assertEquals(time(),$doc['updatetime']);

        $this->assertTrue($result);
        $this->assertInstanceOf('MongoId',$doc['_id']);

        $u = $this->query->find($doc['_id']);
        $this->assertInstanceOf(get_class($this->user),$u);

        $this->query->where(array('_id'=>$doc['_id']))->delete();

    }

    /**
     * testBatchSave
     * @return void
     * @author hwz
     **/
    public function testBatchSave() {
        $docs = array(
            array( 'phone'=>456,'username'=>'coderhwz'),
            array( 'phone'=>456,'username'=>'coderhwz'),
            array( 'phone'=>456,'username'=>'coderhwz'),
        );
        $result = $this->query->batchSave($docs);
        $this->assertEquals(3,$result);
        User::where(array('phone'=>456))->delete();

        foreach ($docs as $d) {
            $this->query->where(array('_id'=>$d['_id']))->delete();
        } 
    }

    /**
     * testInsert
     * @return void
     * @author hwz
     **/
    public function testInsert() {
        $data = array(
            'username'=>'coderhwz',
            'phone'=>'141414114',
        );
        $this->query->insert($data);
        $this->assertInstanceOf('MongoId',$data['_id']);
        $this->query->where(array('_id'=>$data['_id']))->delete();


        $data = array(
            '_id'=>1948483,
            'username'=>'coderhwz',
            'phone'=>'141414114',
        );
        $this->query->insert($data);
        $this->assertEquals(1948483,$data['_id']);
        User::find($data['_id'])->delete();

    }


    /**
     * testFind
     * @return void
     * @author hwz
     **/
    public function testFind() {
        $doc = array('phone'=>789,'username'=>'hello');
        $this->query->save($doc);

        $result = $this->query->find($doc['_id']);
        $this->assertInstanceOf(get_class($this->user),$result);

        $this->query->where(array('_id'=>$doc['_id']))->delete();


        $doc = array('phone'=>789,'username'=>'hello');
        $this->query->save($doc);
        $result = $this->query->find($doc['_id'],array('phone'));
        $this->assertNull($result->username);
        $this->query->where(array('_id'=>$doc['_id']))->delete();

    }

    /**
     * testSkip
     * @return void
     * @author hwz
     **/
    /* public function testSkip() {
        $docs = array(
            array('phone'=>1,'username'),
            array('phone'=>2,'username'),
            array('phone'=>3,'username'),
            array('phone'=>4,'username'),
            array('phone'=>5,'username'),
        );
        $this->query->save($docs);
        $x = $this->query->skip(2)->get(array('phone'));
        $this->assertInstanceOf('MongoLCollection',$x);

        foreach ($x as $xx) {
            $this->assertInstanceOf('MongoLModel',$xx);
        } 

        $this->assertEquals($x->column('phone'),array(3,4,5));

        foreach ($docs as $d) {
            $this->query->where(array('_id'=>$d['_id']))->delete();
        }
        
    } */

    /**
     * testTake
     * @return void
     * @author hwz
     **/
    public function testTake() {
         $docs = array(
            array('phone'=>1,'username'),
            array('phone'=>2,'username'),
            array('phone'=>3,'username'),
            array('phone'=>4,'username'),
            array('phone'=>5,'username'),
        );
        $this->query->batchSave($docs);
        $x = $this->query->take(2)->get(array('phone'));
        $this->assertInstanceOf('MongoLCollection',$x);

        foreach ($x as $xx) {
            $this->assertInstanceOf('MongoLModel',$xx);
        } 

        $this->assertEquals($x->count(),2);

        $this->assertEquals($x->column('phone'),array(1,2));

        User::where(array('$in'=>array(1,2,3,4,5)))->delete();
        foreach ($docs as $d) {
            $this->query->where(array('_id'=>$d['_id']))->delete();
        } 
       
    }

    /**
     * testOrderBy
     * @return void
     * @author hwz
     **/
    public function testOrderBy() {
        /* $user = new User();
        $query = new MongoLQueryBuilder($user);
        $users = $query->orderBy('phone',-1)->get(array('phone'));
        foreach ($users as $u) {
            if (isset($u['phone'])) {
                echo $u['phone'] . chr(10);
            }
        } */
    }

    /**
     * testIncrease
     * @return void
     * @author hwz
     **/
    public function testIncrease() {
        $order = new Order();
        $order->goodsname = '洗衣机';
        $order->count = 10;
        $order->save();
        $this->assertTrue(is_integer($order->_id));

        $count = Order::where(array('_id'=>$order->_id))->increase('count');
        $this->assertEquals(11,$count);

        $count = Order::where(array('_id'=>$order->_id))->decrease('count');
        $this->assertEquals(10,$count);
        $order->delete();

    }

    /**
     * testPagenation
     * @return void
     * @author hwz
     **/
    public function testPagenation() {
        
        for ($i = 0; $i < 99; $i++) {
            $order = new Order();
            $order->goodsname = "恭喜发财";
            $order->fee = $i;
            $order->save();
        }

        $data = Order::where(array('fee'=>array('$gte'=>50)))->pagenation(1,10,'goodsname');
        $this->assertEquals(5,$data['pageCount']);
        $this->assertEquals(1,$data['curPage']);
        $this->assertEquals(10,$data['pageSize']);
        $this->assertEquals(49,$data['total']);
        $this->assertInstanceOf('MongoLCollection',$data['records']);
        for ($i = 0; $i < 99; $i++) {
            Order::where(array('fee'=>$i))->delete();
        }
    }
}


//end of file
