<?php 

/**
 * Class MongoLCollection
 * @author hwz
 */
class MongoLCollection implements Iterator {

    /**
     * 数据源
     *  
     *  
     * @var array
     */
    protected $source;

    /**
     * 模型名称
     *
     * @var string
     */
    protected $class;

    /**
     * 当前索引位置
     *
     * @var int
     */
    protected $position ;

    /**
     * 构造
     * @param array $source 
     * @param string $class
     * @return void
     * @author hwz
     **/
    public function __construct(array $source,$class) {

        $this->position = 0;
        $this->source = $source;
        $this->class = $class;

    }


    /**
     * 获得当前元素
     *
     * @return $class
     * @author hwz
     **/
    public function current() {
        return new $this->class($this->source[$this->position]);
    }

    /**
     * 当前索引 
     *
     * @return int
     * @author hwz
     **/
    public function key() {
        return $this->position;
    }

    /**
     * 获得下一个元素
     *
     * @return $class
     * @author hwz
     **/
    public function next() {
        $this->position ++ ;
    }

    /**
     * rewind
     *
     * @return void
     * @author hwz
     **/
    public function rewind() {
        $this->position = 0;
    }


    /**
     * valid
     * @return void
     * @author hwz
     **/
    public function valid() {
        return isset($this->source[$this->position]);
    }

    /**
     * 获得集合中的某一列
     *
     * @param string $name 列名
     * @return void
     * @author hwz
     **/
    public function column($name) {
        $array = array();
        foreach ($this->source as $value) {
            $array[] = $value[$name];
        }
        return $array;
    }

    /**
     * 集合元素数量 
     *
     * @return void
     * @author hwz
     **/
    public function count() {
        return count($this->source);
    }

    /**
     * 集合中的第一个元素
     *
     * @return array
     * @author hwz
     **/
    public function first() {
        if (isset($this->source[0])) {
            return $this->source[0];
        }
        return NULL;
    }

    /**
     * 获得集合中最后一个元素
     *
     * @return void
     * @author hwz
     **/
    public function last() {
        if (!$this->isEmpty()) {
            return $this->source[$this->count()-1];
        }
        return NULL;
    }

    /**
     * 判断集合是否为空
     *
     * @return boolean
     * @author hwz
     **/
    public function isEmpty() {
        return $this->count() < 1 ;
    }

    /**
     * 求某列的和
     *
     * @return void
     * @author hwz
     **/
    public function sum($column) {
        return array_sum($this->column($column));
    }


    /**
     * toArray
     * @return void
     * @author hwz
     **/
    public function toArray() {
        return $this->source;
    }

    /**
     * toJSON
     * @return void
     * @author hwz
     **/
    public function toJSON() {
        return json_encode($this->source);
    }


    /**
     * append
     * @return void
     * @author hwz
     **/
    public function append($thing) {
        if (!is_array($thing)) {
            $thing = $thing->toArray();
        }
        $this->source[] = $thing;
        $this->rewind();
    }
}


//end of file
