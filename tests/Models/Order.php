<?php 


/**
 * Class Order
 * @author hwz
 */
class Order extends MongoLModel
{
    protected $collectionName = 'order';
    public $autoIncrement = true;

    /**
     * ownner
     * @return void
     * @author hwz
     **/
    public function ownner() {
        return $this->belongsTo('User','user_id','_id');
    }


}


//end of file
