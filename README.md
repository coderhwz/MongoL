#MongoL 


## 简介

```
|~src/
| |-MongoLAutoIncrement.php  主键自增管理
| |-MongoLCollection.php  数据集
| |-MongoLConnection.php  Mongo连接管理器
| |-MongoLException.php   异常
| |-MongoLModel.php     模型父类
| `-MongoLQueryBuilder.php 查询构建器，数据操作

```

## 使用


### 引入Autoload.php 
`require_once('Autoload.php')`
所有MongoL库文件自动加载

### 创建连接

```php
<?php 
    $config = array(
        'host'=>'localhost',
        'port'=>27017,
        'dbname'=>'users',
    );

    new MongoLConnection($config,'连接名');

    // 获得连接
    $connection = MongoLConnection::get('连接名');
    $connection->database; // users
```

### 定义Model

```php
<?php 

/**
 * Class User
 * @author hwz
 */
class User extends MongoLModel
{
    public $collectionName = 'users';

    
    // 开启 自增ID作为主键
    public $autoIncrement = true;

    // 开启软删除，调用delete时，仅添加deletetime
    // 查询时过滤有deletetime的数据
    // 没有开启则是硬删除
    public $softDelete = true;


    /**
     * 每个用户都有一个profile
     *
     * @return Profile
     * @author hwz
     **/
    public function profile() {
        return $this->hasOne('Profile','_id','user_id');
    }

    /**
     * 用户的订单信息
     * 
     * @return MongoLCollection of Order Model
     * @author hwz
     **/
    public function orders() {
        return $this->hasMany('Order','_id','user_id');
    }
}


//end of file
```

### 使用API

数据操作

```php
<?php 
    $user = new User();
    $user->username = "John Meson";
    $user->phone = '1515812899';
    $user->status = 1;
    // 插入
    $user->save();

    // 从表插入
    $user->profile()->save(array('age'=>33));


    // 从表批量插入
    $user->orders()->batchSave(array(
        array('goodsname'=>'Note Book 1',fee=>100),
        array('goodsname'=>'Note Book 2',fee=>100),
        array('goodsname'=>'Note Book 3',fee=>100),
    ));

    var_export($user['_id']);

    // 更新
    $user->phone = '123';

    // 从表数据删除 1对1
    $user->profile->delete();

    // 从表批量删除数据 1对多
    $count = $user->orders()->delete();

    //删除
    $user->delete();


    // 计数增加,更新至数据库
    User::where(array('_id'=>1))->increase('view');
    User::where(array('_id'=>1))->decrease('view');


```

统一使用静态方式查询

```php
<?php
    $user = User::find(1,"username _id");

    // 属性访问返回Profile实例
    var_export($user->profile);

    // 方法访问返回 Order 的查询构建器
    var_export($user->orders());


    $users =  User::where(array('status'=>1))->get('username _id');

    $users1 = User::where(array('status'=>1))->skip(10)->take(10);

    $pagenationData = User::where(array('status'=>1))->pagenation();

```


数据集操作

```php
<?php
    $users = User::get();

    if( !$users->isEmpty() ){
        foreach($users => $user){
            $users->status = 2;
            $user->save();
        }
    }
    //数量
    $users->count();
    // 获得最后一个
    $users->last();
    // 获得第一个
    $users->first();
    // 获得某列
    $users->column('phone');

```

## 未完成 
1. 数据验证
2. 软删除
3. 生命周期内的缓存

