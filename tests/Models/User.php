<?php 

/**
 * Class User
 * @author hwz
 */
class User extends MongoLModel
{
    public $collectionName = 'users';

    public $timestamp = true;


    /**
     * profile
     * @return void
     * @author hwz
     **/
    public function profile() {
        return $this->hasOne('Profile','_id','user_id');
    }

    /**
     * orders
     * @return void
     * @author hwz
     **/
    public function orders() {
        return $this->hasMany('Order','_id','user_id');
    }
}


//end of file
